import 'package:flutter/cupertino.dart';
import 'package:pokedex/src/utils/context.dart';
import 'package:pokedex/src/views/components/pokeball_background.dart';


class ListSearchView extends StatefulWidget {

  const ListSearchView();

  @override
  _ListSearchView createState() => _ListSearchView();
}

class _ListSearchView extends State<ListSearchView> with SingleTickerProviderStateMixin {

  Animation<double> _fabAnimation;
  AnimationController _fabController;
  bool _isFabMenuVisible = false;
  @override
  Widget build(BuildContext context) {
    return PokeballBackground(
      child: Stack(
        children: [

        ],
      ),
    );
  }



  @override
  void dispose() {
    _fabController?.dispose();

    super.dispose();
  }

  void _toggleFabMenu() {
    _isFabMenuVisible = !_isFabMenuVisible;

    if (_isFabMenuVisible) {
      _fabController.forward();
    } else {
      _fabController.reverse();
    }
  }





}