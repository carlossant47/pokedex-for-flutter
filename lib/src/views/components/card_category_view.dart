import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pokedex/src/models/Category.dart';
import 'package:pokedex/src/utils/app_images.dart';
import 'package:pokedex/src/utils/app_theme.dart';
import 'package:pokedex/src/utils/hec_color.dart';

class CardCategoryView extends StatelessWidget {
  const CardCategoryView(
      this.category, {
        this.onPress,
      });

  final Category category;
  final Function onPress;

  Widget _buildCircleDecoration({@required double height}) {
    return Positioned(
      top: -height * 0.616,
      left: -height * 0.53,
      child: CircleAvatar(
        radius: (height * 1.03) / 2,
        backgroundColor: Colors.white.withOpacity(0.14),
      ),
    );
  }

  Widget _buildPokemonDecoration({@required double height}) {
    return Positioned(
      top: -height * 0.16,
      right: -height * 0.25,
      child: Image(
        image: AppImages.pokeball,
        width: height * 1.388,
        height: height * 1.388,
        color: Colors.white.withOpacity(0.14),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constrains) {
        final itemHeight = constrains.maxHeight;
        final itemWidth = constrains.maxWidth;

        return Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.bottomCenter,

            ),
            Material(
              color: HexColor(category.colorBackground),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
              clipBehavior: Clip.antiAlias,
              child: InkWell(
                splashColor: Colors.white10,
                highlightColor: Colors.white10,
                onTap: onPress,
                child: Stack(
                  children: [
                    _buildPokemonDecoration(height: itemHeight),
                    _buildCircleDecoration(height: itemHeight),
                    _CardContent(category.title),
                  ],
                ),
              ),
            )
          ],
        );
      },
    );
  }
}

class _CardContent extends StatelessWidget {
  const _CardContent(this.name);

  final String name;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Text(
          name,
          style: AppTheme.display3White,
        ),
      ),
    );
  }
}


