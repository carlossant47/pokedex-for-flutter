import 'package:flutter/material.dart';
import 'package:pokedex/src/utils/app_theme.dart';
import 'package:pokedex/src/utils/context.dart';
import 'package:pokedex/src/utils/spacer.dart';

class SearchBar extends StatelessWidget {



  const SearchBar({
    this.margin = const EdgeInsets.symmetric(horizontal: 28),
  });


  void searchPokemon(String name) {
    print(name);
  }

  final EdgeInsets margin;

  @override
  Widget build(BuildContext context) {
    final TextEditingController pokemonInputName = TextEditingController();
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 18, vertical: context.responsive(8)),
      margin: margin,
      decoration: ShapeDecoration(
        shape: StadiumBorder(),
        color: Theme.of(context).scaffoldBackgroundColor,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(Icons.search, size: context.responsive(26)),
          HSpacer(13),
          Expanded(
            child: TextField(
              style: TextStyle(
                fontSize: 20,
                color: AppTheme.grey,
                height: 1,
              ),
              onSubmitted: (String value) async {
                searchPokemon(value);
              },

              controller: pokemonInputName,
              decoration: InputDecoration(
                isDense: true,
                hintText: 'Search Pokemon, Move, Ability etc',
                contentPadding: EdgeInsets.symmetric(),
                hintStyle: TextStyle(
                  fontSize: 20,
                  color: AppTheme.grey,
                  height: 1,
                ),
                border: InputBorder.none,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
