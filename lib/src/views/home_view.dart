import 'package:flutter/material.dart';
import 'package:pokeapi/pokeapi.dart';

import 'package:pokedex/routes.dart';
import 'package:pokedex/src/models/Category.dart';
import 'package:pokedex/src/views/components/card_category_view.dart';
import 'package:pokedex/src/views/components/search_bar.dart';
import 'components/pokeball_background.dart';
import 'package:pokedex/src/utils/spacer.dart';
import 'package:pokedex/src/utils/context.dart';
part 'components/header_app.dart';

class HomeView extends StatefulWidget {
  const HomeView({Key key}) : super(key: key);

  @override
  _HomeView createState() => _HomeView();
}

class _HomeView extends State<HomeView> with TickerProviderStateMixin {
  final ScrollController _scrollController = ScrollController();

  double appBarHeight = 0;
  bool showTitle = false;
  AnimationController animationController;



  @override
  void initState() {

    _scrollController.addListener(_onScroll);

    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
  }

  @override
  void dispose() {
    _scrollController.dispose();

    super.dispose();
  }

  void _onScroll() {
    if (!_scrollController.hasClients) return;

    final offset = _scrollController.offset;

    setState(() {
      showTitle = offset > appBarHeight - kToolbarHeight;
    });
  }

  void getAllPokemons() {

  }

  @override
  Widget build(BuildContext context) {
    appBarHeight = context.screenSize.height * _HeaderAppBar.heightFraction;
    return Scaffold(
      body: NestedScrollView(
        controller: _scrollController,
        headerSliverBuilder: (_, __) => [
          _HeaderAppBar(
            height: appBarHeight,
            showTitle: showTitle,
          ),
        ],
        body: Container(),
      ),
    );
  }



}
